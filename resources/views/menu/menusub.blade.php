@foreach($childs as $child)
@if($child->children->isEmpty())
<li class="dropdown-item" href="#">
    <a href="{{ $child->link() }}">{{ $child->title }}</a>
</li>
@else
<li class="dropdown-submenu">
    <a class="dropdown-item dropdown-toggle" href="#">
        {{ $child->title }}
    </a>
    <ul class="dropdown-menu">
        @include('menu.menusub',['childs' => $child->children,'id_sub' =>$child->id])
    </ul>
</li>
@endif
@endforeach
