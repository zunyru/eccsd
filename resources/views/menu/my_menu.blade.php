<ul class="navbar-nav {{ setting('site.menu-bar-position') }}">
    @php
    if (Voyager::translatable($items)) {
    $items = $items->load('translations');
    }
    @endphp
    @foreach ($items as $menu_item)
    @php
    $originalItem = $menu_item;
    if (Voyager::translatable($menu_item)) {
    $menu_item = $menu_item->translate($options->locale);
    }
    $isActive = null;
    $styles = null;
    $icon = null;
    // Background Color or Color
    if (isset($options->color) && $options->color == true) {
    $styles = 'color:'.$menu_item->color;
    }
    if (isset($options->background) && $options->background == true) {
    $styles = 'background-color:'.$menu_item->color;
    }
    // Check if link is current
    if(url($menu_item->link()) == url()->current()){
    $isActive = 'active';
    }
    if(url($menu_item->link()) == url('/').'/'.request()->segment(1)){
    $isActive = 'active';
    }
    // Set Icon
    if(isset($options->icon) && $options->icon == true){
    $icon = '<i class="' . $menu_item->icon_class . '"></i>';
    }
    @endphp
    @if(isset($isActive))
    <li class="nav-item {{$isActive}}">
        <a class="nav-link" href="{{ $menu_item->link() }}">{{ $menu_item->title }}
            <span class="sr-only">(current)</span>
        </a>
    </li>
    @else
    <li class="nav-item">
        <a class="nav-link" href="{{ $menu_item->link() }}">{{ $menu_item->title }}
        </a>
    </li>
    @endif
    @if(!$originalItem->children->isEmpty())
    @endif
    @endforeach
</ul>
