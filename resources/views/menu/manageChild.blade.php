<ul>
    @foreach($originalItem->children as $child)
    <li>
        {{ $child->title }}
        @if(count($child->childs))
        @include('manageChild',['childs' => $child->childs])
        @endif
    </li>
    @endforeach
</ul>
