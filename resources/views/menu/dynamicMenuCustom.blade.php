@php
    if (Voyager::translatable($items)) {
        $items = $items->load('translations');
    }
@endphp

<nav class="navbar navbar-expand-lg navbar-dark shadow-sm py-0">
    <a class="navbar-brand d-flex justify-content-between" href="{{ url('/') }}">
        <img src="{{ Voyager::image( setting('site.logo') ) }}" alt="" width="20">
        <span>eccsd</span>
    </a>

    <div>
{{--        <a href="{{ route('pages.donate') }}" class="text-uppercase text-warning text-smaller mr-1 navbar-expand-lg navbar-toggler"><i class="fas fa-donate"></i> Donate</a>--}}
        <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbars" aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler order-md-1">
            <i class="fas fa-bars"></i>
        </button>
    </div>

    <div id="navbarContent" class="collapse navbar-collapse order-sm-12 order-lg-1">
        <ul class="navbar-nav">
            @foreach($items as $key => $menu)
                @php
                    $isActive = '';
                    if(url($menu->link()) == url()->current()){
                    	$isActive = 'active';
                    }
                    if(url($menu->link()) == url('/').'/'.request()->segment(1)){
                    	$isActive = 'active';
                    }
                @endphp
                @if(!$items[$key]->children->isEmpty())
                    <li class="nav-item dropdown megamenu">
                        <a id="megamenu" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                           class="nav-link dropdown-toggle">
                            {{ $menu->title }}
                        </a>
                        @if(!$menu->children->isEmpty())
                            <div aria-labelledby="megamenu" class="dropdown-menu border-0 p-0 m-0">
                                <div class="container-fluid">
                                    <div class="row bg-dark-custom text-light rounded-0 m-0 shadow-sm container-fluid">
                                        <div class="col-12 pt-4 megamenu-box-content">
                                            <div class="row">
                                                @foreach($menu->children as $menu_sub)
                                                    @if($menu_sub->children->isEmpty())
                                                        <div class="col-sm-6 col-lg-3 mb-4">
                                                            <a href="{{ url($menu_sub->link())  }}" class="text-white">
                                                                <h6>{{ $menu_sub->title }}</h6>
                                                            </a>
                                                        </div>
                                                    @endif
                                                    @if(!$menu_sub->children->isEmpty())
                                                        <div class="col-sm-6 col-lg-3 mb-4">
                                                            <h6>{{ $menu_sub->title }}</h6>
                                                            <ul class="list-icons">
                                                                @foreach($menu_sub->children as $menu_sub_detail)
                                                                    <li class="nav-item">
                                                                        <a href="{{ url($menu_sub_detail->link()) }}" class="nav-link text-small">
                                                                            <i class="fas fa-chevron-right"></i> {{ $menu_sub_detail->title }}
                                                                        </a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </li>
                @endif
                @if($items[$key]->children->isEmpty())
                    <li class="nav-item">
                        <a href="{{ $menu->link() }}" class="nav-link">
                            {{ $menu->title }}
                        </a>
                    </li>
                @endif
            @endforeach
        </ul>
{{--        <ul class="navbar-nav ml-auto topbar">--}}
{{--            <li class="nav-item">--}}
{{--                <a href="{{ route('pages.donate') }}" class="nav-link btn-donate">--}}
{{--                    <i class="fas fa-donate"></i>--}}
{{--                    Donate--}}
{{--                </a>--}}
{{--            </li>--}}
{{--        </ul>--}}
    </div>
</nav>


