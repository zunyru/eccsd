@extends('frontend.main')
@section('title', setting('site.title'))
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
@isset($banners)
{{-- @include('frontend.slide.banner-video')  --}}
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')
<main class="mt-5">
    <div class="container service">
        @isset($services)
        <h1 class="h3 text-left mb-3">บริการ</h1>
        <div class="row">
            @foreach ($services as $service)
            <!-- Grid column -->
            <div class="col-lg-4 mb-5">
                @if(!empty($service->image))
                <a class="d-block rounded-lg lift lift-lg" href="{{ route('services.show',$service->slug) }}">
                    <img src="{{ Voyager::image($service->thumbnail('medium')) }}" class="img-fluid rounded-lg"
                        alt="{{ $service->title }}">
                </a>
                @endif
                <a href="{{ route('services.show',$service->slug) }}">
                    <h1 class="h5 text-center mt-3">{{ $service->title }}</h1>
                </a>
            </div>
            <!-- Grid column -->
            @endforeach
        </div>
        @endisset
    </div>
</main>
@stop
@section('footer')
@include('frontend.layouts.footer')
@stop
