<header class="blog-header py-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="d-flex justify-content-lg-start">
                    <a class="blog-header-logo text-dark" href="{{ url('/') }}">
                        {{--<img class="rounded" src="{{ Voyager::image( setting('site.logo') ) }}" width="70" alt="eccsd logo">--}}
                        <img src="{{ asset('images/logo_eccsd.png') }}" width="550px">
                    </a>
                </div>
                {{--<div class="float-lg-right btn btn-warning ">
                    <a href="{{ route('pages.donate') }}" class="text-uppercase text-black-50"><i class="fas fa-donate"></i>
                        Donate
                    </a>
                </div>--}}
            </div>
            {{--<div class="col-8">
                <div class="float-right">
                    <a class="" href="{{ url('/') }}">
                        News
                    </a>
                    |
                    <a class="" href="{{ url('/') }}">
                        Blogs
                    </a>
                </div>
            </div>--}}
        </div>
    </div>
</header>


{{ menu('web','menu.dynamicMenuCustom') }}

{{--
<nav class="navbar navbar-expand-lg navbar-dark shadow-sm ">
    <button type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbars"
            aria-expanded="false" aria-label="Toggle navigation" class="navbar-toggler order-md-1">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div id="navbarContent" class="collapse navbar-collapse order-sm-12 order-lg-1">
        <ul class="navbar-nav mr-auto">
            <!-- Megamenu-->
            <li class="nav-item dropdown megamenu">
                <a id="megamenu" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                   class="nav-link dropdown-toggle font-weight-bold text-uppercase">
                    Dropdown Menu
                </a>
                <div aria-labelledby="megamenu" class="dropdown-menu border-0 p-0 m-0">
                    <div class="container-fluid">
                        <div class="row bg-dark-custom text-light rounded-0 m-0 shadow-sm container-fluid">
                            <div class="col-12">
                                <div class="p-4">
                                    <div class="row">
                                        <div class="col-sm-6 col-lg-3 mb-4">
                                            <h6 class="font-weight-bold text-uppercase">Heading 1</h6>
                                            <ul class="list-unstyled">
                                                <li class="nav-item"><a href="#" class="nav-link text-small pb-0">Unique
                                                        Features</a></li>
                                                <li class="nav-item"><a href="#" class="nav-link text-small pb-0 ">Image
                                                        Responsive</a></li>
                                                <li class="nav-item"><a href="#" class="nav-link text-small pb-0 ">Auto
                                                        Carousel</a></li>
                                                <li class="nav-item"><a href="#" class="nav-link text-small pb-0 ">Newsletter
                                                        Form</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 col-lg-3 mb-4">
                                            <h6 class="font-weight-bold text-uppercase">Heading 2</h6>
                                            <ul class="list-unstyled">
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Unique
                                                        Features</a></li>
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Image
                                                        Responsive</a></li>
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Auto
                                                        Carousel</a></li>
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Newsletter
                                                        Form</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 col-lg-3 mb-4">
                                            <h6 class="font-weight-bold text-uppercase">Heading 3</h6>
                                            <ul class="list-unstyled">
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Unique
                                                        Features</a></li>
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Image
                                                        Responsive</a></li>
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Auto
                                                        Carousel</a></li>
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Newsletter
                                                        Form</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6 col-lg-3 mb-4">
                                            <h6 class="font-weight-bold text-uppercase">Heading 4</h6>
                                            <ul class="list-unstyled">
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Unique
                                                        Features</a></li>
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Image
                                                        Responsive</a></li>
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Auto
                                                        Carousel</a></li>
                                                <li class="nav-item"><a href="" class="nav-link text-small pb-0 ">Newsletter
                                                        Form</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <li class="nav-item"><a href="" class="nav-link font-weight-bold text-uppercase">About</a></li>
            <li class="nav-item"><a href="" class="nav-link font-weight-bold text-uppercase">Services</a></li>
            <li class="nav-item"><a href="" class="nav-link font-weight-bold text-uppercase">Contact</a></li>
        </ul>
    </div>
</nav>
--}}
