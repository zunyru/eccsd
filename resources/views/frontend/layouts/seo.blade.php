<meta name="description" content="{{ $dataTypeContent->meta_description ??  setting('web-seo.description') }}" />
<meta name="keywords" content="{{ $dataTypeContent->meta_keywords ?? setting('web-seo.keyword') }}" />
<meta name="author" content="Sansu Sa Ma Air" />
<!-- Open Graph data -->
<meta property="og:url" content="{{ url()->full() }}" />
<meta property="og:type" content="webiste" />
<meta property="og:title" content="{{ $dataTypeContent->meta_title ?? setting('web-seo.title') }}" />
<meta property="og:description" content="{{ $dataTypeContent->meta_description ?? setting('web-seo.description') }}" />
<meta property="og:image"
    content="{{ (isset($dataTypeContent->image) && Voyager::image($dataTypeContent->image)) ? Voyager::image($dataTypeContent->image) : Voyager::image(setting('web-seo.image')) }}" />
<meta property="og:site_name" content="{{ $dataTypeContent->title ?? setting('web-seo.title') }}" />
<!-- Twitter Card data -->
<meta name="twitter:card" content="{{ $dataTypeContent->meta_title ??  setting('web-seo.title') }}">
<meta name="twitter:site" content="{{ url()->full() }}">
<meta name="twitter:title" content="{{ $dataTypeContent->meta_title ?? setting('site.title') }}">
<meta name="twitter:description" content="{{ $dataTypeContent->meta_description ?? setting('web-seo.description') }}">
<meta name="twitter:creator" content="">
<!-- Twitter Summary card images must be at least 120x120 -->
<meta name="twitter:image"
    content="{{ (isset($dataTypeContent->image) && Voyager::image($dataTypeContent->image)) ? Voyager::image($dataTypeContent->image)  : Voyager::image(setting('web-seo.image')) }}">
