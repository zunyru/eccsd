<!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<!-- Bootstrap core CSS -->
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
<!-- Your custom styles (optional) -->
<link href="{{ asset('css/style.min.css') }}" rel="stylesheet">
<link href="{{ mix('css/custom.css') }}" rel="stylesheet">
{{-- fancybox --}}
<link rel="stylesheet" type="text/css" href="{{ asset('node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.css') }}" media="screen"/>
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('plugin/fancybox/source/jquery.fancybox.css') }}"
media="screen" /> --}}
@if(setting('site.google-font'))
    <link href="https://fonts.googleapis.com/css?family={{ setting('site.google-font') }}:400,500,700&display=swap"
          rel="stylesheet">
    <style type="text/css">
        html,
        body,
        header,
        .view {
            font-family: "{{ setting('site.google-font') }}";
        }
    </style>
@endif
<style type="text/css">
    {{-- Custon css --}}
    @if(setting('css.custom_css'))
    {!! setting('css.custom_css') !!}
    @endif
 /* Necessary for full page carousel*/
    html,
    body,
    header,
    .view {
        height: auto;
    }

    body {
        color: @php echo setting('site.text_body_color')? setting('site.text_body_color'): '#212529'@endphp;
        background: @php echo setting('site.body_background_color')? setting('site.body_background_color'): ''@endphp;
    }

    a {
        color: @php echo setting('site.text_link_color')? setting('site.text_link_color'): '#007bff'@endphp;
    }

    .grey-text {
        color: @php echo setting('site.text_sub_color')? setting('site.text_sub_color').'!important': '#9e9e9e
!important;'@endphp;
    }

    .show > .btn-primary.dropdown-toggle,
    .btn-primary {
        background-color: @php echo setting('site.button_color')? setting('site.button_color').'!important': '#4285f4
!important;'@endphp;
        color: @php echo setting('site.button_text_color')? setting('site.button_text_color'): '#fff'@endphp;;
    }

    .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active,
    .show > .btn-primary.dropdown-toggle,
    .btn-primary:not([disabled]):not(.disabled).active {
        background-color: @php echo setting('site.button_color')? hex2rgba(setting('site.button_color'),0.6).'!important':
'#0b51c5!important;'@endphp;
    }

    a:hover {
        color: @php echo setting('site.text_link_color')? hex2rgba(setting('site.text_link_color'),0.6):
'#0b51c5;'@endphp;
    }

    /* Carousel*/
    .carousel,
    .carousel-item,
    .carousel-item.active {
        height: 100%;
    }

    .carousel-inner {
        height: 100%;
    }

    @php $color = setting('site.menu_bar_background_color') ? setting('site.menu_bar_background_color') : '#1C2331'; @endphp
.form-control:focus {
        border-color: {{ setting('site.menu_bar_background_color') ? setting('site.menu_bar_background_color') : '#1C2331' }};
        outline: 0;
        box-shadow: 0 0 0 0.2rem{{ hex2rgba($color,0.25) }};
    }

    @media (min-width: 800px) and (max-width: 850px) {
        .navbar:not(.top-nav-collapse) {
            background: #1C2331 !important;
        }
    }
    @stack('css')
</style>
