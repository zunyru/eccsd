<!-- JQuery -->
<script type="text/javascript" src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>
{{-- fancybox --}}
<script type="text/javascript" src="{{ asset('node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js') }}">
</script>
{{-- <script type="text/javascript" src="{{ asset('plugin/fancybox/source/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugin/fancybox/source/helpers/jquery.fancybox-buttons.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugin/fancybox/source/helpers/jquery.fancybox-media.js') }}"></script>
--}}
<!-- Initializations -->
<script type="text/javascript">
    // Animations initialization
    new WOW().init();
</script>
<script src="{{ asset('node_modules/jquery-countdown/dist/jquery.countdown.js') }}"></script>
<script>
    var datetime = $('#clock').attr('data-datetime');
    $('#clock').countdown(datetime)
        .on('update.countdown', function(event) {
        var format = '%H:%M:%S';
        if(event.offset.totalDays > 0) {
            format = '%-d วัน ' + format;
        }
        if(event.offset.weeks > 0) {
            format = '%-w สัปดาห์ ' + format;
        }
        $(this).html(event.strftime(format));
        })
        .on('finish.countdown', function(event) {
        $(this).html('โปรหมดแล้ว!')
            .parent().addClass('disabled');
        });

    $(document).ready(function() {
        $( ".card" ).hover(
            function() {
                $(this).addClass('shadow').css('cursor', 'pointer');
            }, function() {
                $(this).removeClass('shadow');
            }
        );
    });
</script>
{{-- Custon js --}}
@if(setting('js.custom_js'))
{!! setting('js.custom_js') !!}
@endif
