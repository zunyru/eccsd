<!-- Footer -->
<footer class="page-footer font-small teal pt-2 bg-main text-white">

    @if(!empty(setting('site.footer1')) && !empty(setting('site.footer2')))
    <!-- Footer Text -->
    <div class="container-fluid text-center text-md-left">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-md-6 mt-md-0 mt-3">

                <!-- Content -->
               {!! setting('site.footer1') !!}

            </div>
            <!-- Grid column -->

            <hr class="clearfix w-100 d-md-none pb-3">

            <!-- Grid column -->
            <div class="col-md-6 mb-md-0 mb-3">

                <!-- Content -->
               {!! setting('site.footer2') !!}

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

    </div>
    <!-- Footer Text -->
    @endif
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">
        <p>Copyright Notice © 2020 {{ env('APP_NAME') }} All rights reserved.</p>
        <!-- Histats.com  (div with counter) -->
        <div id="histats_counter"></div>
        <!-- Histats.com  START  (aync)-->
        <script type="text/javascript">var _Hasync= _Hasync|| [];
            _Hasync.push(['Histats.start', '1,4447120,4,2047,280,25,00011000']);
            _Hasync.push(['Histats.fasi', '1']);
            _Hasync.push(['Histats.track_hits', '']);
            (function() {
                var hs = document.createElement('script'); hs.type = 'text/javascript'; hs.async = true;
                hs.src = ('//s10.histats.com/js15_as.js');
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(hs);
            })();</script>
        <!-- Histats.com  END  -->
    </div>
</footer>
<!-- Footer -->

