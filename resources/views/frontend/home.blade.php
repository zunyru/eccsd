@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
    @include('frontend.layouts.seo')
@stop
@section('navbar')
    @include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
    @isset($banners)
        @include('frontend.slide.banner-image')
    @endisset
@stop
@section('content')
    <main class="main-section-home">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 bg-dark-custom text-center p-5">
                    @if(!empty(setting('site.jumbotron_text')))
                        <h1 class="font-veneer text-white">
                            {{ setting('site.jumbotron_text') }}
                        </h1>
                    @endif
                </div>
            </div>
            <div class="row">
                @if(!empty($pages))
                    <div class="col-lg-6 col-md-6 col-sm-12 pb-3">
                        <h2 class="text-uppercase font-veneer pt-5 pb-1 px-3">Update</h2>
                        <div class="row">
                            @foreach($pages as $key => $item)
                                @if($key == 0)
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <a href="{{ route('pages.detail',$item->slug) }}">
                                            <div class="card card-index rounded-0 border-0">
                                                <div class="card-img-wrap">
                                                    <img src="{{ Voyager::image($item->image) }}" class="card-img-top rounded-0" alt="">
                                                </div>
                                                <div class="card-body">
                                                    <h4 class="card-title font-veneer">{{ $item->title }}</h4>
                                                    <p class="card-text fix-height">{!! $item->excerpt !!}</p>
                                                    <p class="text-uppercase font-veneer pt-2">{{ $item->public_at }}</p>
{{--                                                <h6 class="float-left mt-3">{{ $item->head_title }}</h6>--}}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endif
                                @if($key > 0)
                                    <div class="col-lg-6 col-md-6 col-sm-12 mt-3">
                                        <a href="{{ route('pages.detail', $item->slug) }}">
                                            <div class="card rounded-0 border-0 bg-white text-dark">
                                                <div class="card-img-wrap box-fix">
                                                    <img src="{{ Voyager::image($item->image) }}"  class="card-img-top rounded-0" alt="">
                                                </div>
                                                <div class="card-body">
                                                    <h4 class="card-title font-veneer">{{ $item->title }}</h4>
                                                    <p class="card-text dotellipsis">{!! $item->excerpt !!}</p>
                                                    <p class="text-uppercase font-veneer pt-2">{{ $item->public_at }}</p>
{{--                                                    <h6 class="float-left mt-3">{{ $item->head_title }}</h6>--}}
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <div class="px-3 text-right mt-3">
                            <a href="{{ route('page-all') }}" class="btn btn-outline-secondary btn-sm rounded-0 px-3">ดูเพิ่มเติม <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                @endif
                @if(!empty($highlights))
                    <div class="col-lg-6 col-md-6 col-sm-12 pb-3">
                        <h2 class="text-uppercase font-veneer pt-5 pb-1 px-3">Highlights</h2>
                        <div class="row">
                            @foreach($highlights as $key => $item)
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <a href="{{ route('pages.detail',$item->slug) }}" class="d-block bg-danger mb-3">
                                        <div class="card rounded-0 border-0 bg-white text-dark">
                                            <div class="card-img-wrap box-fix">
                                                <img src="{{ Voyager::image($item->image) }}" class="card-img-top rounded-0" alt="">
                                            </div>
                                            <div class="card-body">
                                                <h4 class="card-title font-veneer">{{ $item->title }}</h4>
                                                <p class="card-text dotellipsis">{!! $item->excerpt !!}</p>
                                                <p class="text-uppercase font-veneer pt-2">{{ $item->public_at }}</p>
{{--                                                <h6 class="float-left mt-3">{{ $item->head_title }}</h6>--}}
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                        <div class="px-3 text-right">
                            <a href="{{ route('page-all') }}" class="btn btn-outline-secondary btn-sm rounded-0 px-3">ดูเพิ่มเติม <i class="fas fa-long-arrow-alt-right"></i></a>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row bg-info">
                @if(!empty(setting('site.text-promotion')))
                    <div class="col-12 text-center text-white font-veneer py-5">
                        <h2 class="mb-0">{!! setting('site.text-promotion') !!}</h2>
                    </div>
                @endif
            </div>
        </div>
    </main>
@stop
@section('footer')
    @include('frontend.layouts.footer')
@stop
