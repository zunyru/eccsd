@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
    @include('frontend.layouts.seo')
@stop
@section('navbar')
    @include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('content')
    <main class="main-section-home">
        <div class="container-fluid">
            <div class="row px-3">
                @foreach($pages as $key => $item)
                <div class="col-lg-3 col-md-3 col-sm-12 pb-3 mt-5 px-0">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <a href="{{ route('pages.detail',$item->slug) }}">
                            <div class="card rounded-0">
                                <div class="card-img-wrap">
                                    <img src="{{ Voyager::image($item->image) }}"
                                         class="card-img-top"
                                         alt="">
                                </div>
                                <div class="card-body">
                                    <h5 class="card-title font-veneer">{{ $item->title }}</h5>
                                    <p class="card-text fix-height dotellipsis">{!! $item->excerpt !!}</p>
                                    <p class="text-uppercase font-veneer pt-2">{{ $item->public_at }}</p>
{{--                                    <h6 class="font-veneer float-left mt-3">{{ $item->head_title }}</h6>--}}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="text-center pagination justify-content-center">
                {{ $pages->withQueryString()->links() }}
            </div>

        </div>
    </main>
@stop
@section('footer')
    @include('frontend.layouts.footer')
@stop
