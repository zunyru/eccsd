<!DOCTYPE html>
<html lang="th">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="robots" content="follow">
    <meta name="googlebot" content="index">
{{--    <?php $admin_favicon = Voyager::setting('site.favicon', ''); ?>--}}
{{--    @if($admin_favicon == '')--}}
{{--        <link rel="shortcut icon" href="{{ voyager_asset('images/logo-icon.png') }}" type="image/png">--}}
{{--    @else--}}
{{--        <link rel="shortcut icon" href="{{ Voyager::image($admin_favicon) }}" type="image/png">--}}
{{--    @endif--}}
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" href="{{ asset('node_modules/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('node_modules/@fortawesome/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ mix('css/custom.css') }}">
    <title>@yield('title')</title>
    @yield('seo')
    <style>
        .waves-ripple{
            display: none;
        }
    </style>
    {!! setting('site.js') !!}

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id={{setting('site.google_analytics_tracking_id')}}"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', '{{setting("site.google_analytics_tracking_id")}}');
    </script>
</head>
<body>

@section('navbar')
@show

@section('slides')
@show
<!-- data-ride="carousel" -->
<!--Main layout-->
@yield('content')
<!--Main layout-->
@section('footer')
@show
<!-- SCRIPTS -->
@include('frontend.layouts.scripts')
@stack('custom-scripts')
</body>

</html>
