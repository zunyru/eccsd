@extends('frontend.main')
@section('title', $article->title ?? setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@if(setting('web-seo.sharethis'))
{!! setting('web-seo.sharethis') !!}
@endif
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')
<main class="mt-5">
    <div class="container articles-detail">
        <section class="content">
            <h1 class="h3 text-left mb-3">{{ $article->title }}</h1>
            <p class="category">หมวดหมู่ : {{ $article->category->name }}</p>
            <small class="post_date">โฟสเมื่อ : {{ DateThai($article->created_at)}}</small>
            <small class="read">อ่าน : {{ number_format($article->viewer + 1 ,0)}}</small>
            {{-- tag --}}
            <section class="mt-3">
                @foreach ($article->tagArray as $tag)
                <a href="{{ route('articles.tag',str_replace(' ','-',$tag))  }}" class="tag">{{ $tag }}</a>
                @endforeach
            </section>
            <div class="row">
                <div class="col-md-8 mb-5 mt-3">
                    <img src="{{ Voyager::image( $article->image) }}" class="img-fluid rounded-lg"
                        alt="{{ $article->title }}">
                    {{-- Share this --}}
                    @if(setting('web-seo.sharethis'))
                    <div class="my-4">
                        <div class="sharethis-inline-share-buttons"></div>
                    </div>
                    @endif
                    <div class="mt-2">
                        {!! $article->body !!}

                        @if(setting('article.article-detail-page-script'))
                            <br>
                            {!! setting('article.article-detail-page-script') !!}
                        @endif
                    </div>
                </div>
                <div class="col-md-4 mt-3">
                    @if(setting('contact.facebook_page'))
                    {!! setting('contact.facebook_page') !!}
                    @endif
                    {{-- Category--}}
                    <div class="mt-3">
                        @if(setting('ads.right'))
                        {!! setting('ads.right') !!}
                        @endif
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@stop
@push('custom-scripts')
<script>
    $(".fancybox").fancybox();
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
