@extends('frontend.main')
@section('title')
    {{ isset($dataTypeContent->meta_title) ? $dataTypeContent->meta_title. ' | '. setting('site.title') : setting('site.title') }}
@stop
@section('seo')
    @include('frontend.layouts.seo')
@stop
@section('navbar')
    @include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
    @isset($banners)
        @include('frontend.slide.banner-image')
    @endisset
@stop
@section('content')
    <main>
        <header class="hero-image" style="background-image:linear-gradient(rgba(0, 0, 0, 0.1), rgba(0, 0, 0, 0.1)),
            url('{{ Voyager::image($dataTypeContent->image) }}');">
            <div class="hero-text">
                <h1 style="font-size:50px">{{ $dataTypeContent->title }}</h1>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    @if(!empty($dataTypeContent->head_title))
                        <h1 class="font-veneer mt-5">{{ $dataTypeContent->head_title }}</h1>
                    @endif
                    @php
                        $dataTypeRows = $dataType->readRows;
                    @endphp
                    @foreach($dataTypeRows as $row)
                        @if ($row->type == 'relationship')
                            @if($row->details->type == 'belongsTo')
                                @php
                                    $relationshipData = (isset($data)) ? $data : $dataTypeContent;
                                    $model = app($row->details->model);
                                    $query = $model::where($row->details->key,$relationshipData->{$row->details->column})->first();
                                @endphp
                                @if(isset($query))
                                    <h6>{{'#'.$query->{$row->details->label} }}</h6>
                                @endif
                            @endif
                        @endif
                    @endforeach

                    @if(setting('site.js'))
                        <div class="my-2">
                            <!-- ShareThis BEGIN -->
                            <div class="sharethis-inline-share-buttons"></div>
                            <!-- ShareThis END -->
                        </div>
                    @endif

                    <div class="content mt-5 blockquote">
                        {!! $dataTypeContent->content !!}
                    </div>

                </div>
            </div>
            <hr>
            <section class="related-pages mt-5 mb-3">
                <div class="mb-4">
                    <h2 class="font-veneer">RELATED PAGES</h2>
                </div>
                <div class="row">
                    @foreach($related as $item)
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <a href="{{ route('pages.detail',$item->slug) }}">
                                <div class="card bg-white text-dark">
                                    <div class="card-img-wrap box-fix">
                                        <img src="{{ Voyager::image($item->image) }}"
                                             class="card-img-top"
                                             alt="">
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $item->title }}</h5>
                                        <p class="card-text dotellipsis">{!! $item->excerpt !!}</p>
                                        <h6 class="font-veneer float-left mt-3">{{ $item->head_title }}</h6>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </section>
        </div>
    </main>
@stop
@section('footer')
    @include('frontend.layouts.footer')
@stop

