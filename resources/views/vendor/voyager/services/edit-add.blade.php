@php
$edit = !is_null($dataTypeContent->getKey());
$add = is_null($dataTypeContent->getKey());
@endphp
@extends('voyager::master')
@section('css')
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- font -->
<link href="{{ asset('plugin/fileuploader/dist/font/font-fileuploader.css') }}" media="all" rel="stylesheet">
<!-- css -->
<link href="{{ asset('plugin/fileuploader/dist/jquery.fileuploader.min.css') }}" media="all" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('plugin/fileuploader/drag-drop/css/jquery.fileuploader-theme-dragdrop.css') }}">
<style>
    .fileuploader-theme-dragdrop .fileuploader-input {
        padding: unset;
    }
</style>
<!-- Add fancyBox main JS and CSS files -->
<link rel="stylesheet" type="text/css"
    href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css" media="screen" />
@stop
@section('page_title', __('voyager::generic.'.($edit ? 'edit' : 'add')).'
'.$dataType->getTranslatedAttribute('display_name_singular'))
@section('page_header')
<h1 class="page-title">
    <i class="{{ $dataType->icon }}"></i>
    {{ __('voyager::generic.'.($edit ? 'edit' : 'add')).' '.$dataType->getTranslatedAttribute('display_name_singular') }}
</h1>
@include('voyager::multilingual.language-selector')
@stop
@section('content')
<div class="page-content edit-add container-fluid">
    <!-- form start -->
    <form role="form" class="form-edit-add"
        action="{{ $edit ? route('voyager.'.$dataType->slug.'.update', $dataTypeContent->getKey()) : route('voyager.'.$dataType->slug.'.store') }}"
        method="POST" enctype="multipart/form-data">
        <!-- PUT Method if we are editing -->
        @if($edit)
        {{ method_field("PUT") }}
        @endif
        <!-- CSRF TOKEN -->
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-8">
                <!-- ### TITLE ### -->
                <div class="panel">
                    @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="voyager-character"></i> {{ 'Service Title' }}
                            <span class="panel-desc"> {{ 'The title for your service' }}</span>
                        </h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                                aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body" id="title_page">
                        @include('voyager::multilingual.input-hidden', [
                        '_field_name' => 'title',
                        '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                        ])
                        <input type="text" class="form-control" id="title" name="title"
                            placeholder="{{ __('voyager::generic.title') }}"
                            value="{{ $dataTypeContent->title ?? '' }}">
                    </div>
                </div>
                <!-- ### CONTENT ### -->
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ 'Service Content' }}</h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-resize-full" data-toggle="panel-fullscreen"
                                aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        @include('voyager::multilingual.input-hidden', [
                        '_field_name' => 'body',
                        '_field_trans' => get_field_translations($dataTypeContent, 'body')
                        ])
                        @php
                        $dataTypeRows = $dataType->{(isset($dataTypeContent->id) ? 'editRows' : 'addRows' )};
                        $row = $dataTypeRows->where('field', 'body')->first();
                        @endphp
                        {!! app('voyager')->formField($row, $dataType, $dataTypeContent) !!}
                    </div>
                </div><!-- .panel -->
                <!-- ### EXCERPT ### -->
                {{-- <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ 'EXCERPT' }}</h3>
                <div class="panel-actions">
                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                </div>
            </div>
            <div class="panel-body">
                @include('voyager::multilingual.input-hidden', [
                '_field_name' => 'excerpt',
                '_field_trans' => get_field_translations($dataTypeContent, 'excerpt')
                ])
                <textarea class="form-control" name="excerpt">{{ $dataTypeContent->excerpt ?? '' }}</textarea>
            </div>
        </div> --}}
        <!-- ### SERVICE PRICE DETAILS  ### -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{ 'Price Detail' }}</h3>
                <div class="panel-actions">
                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <h5 for="status">{{ 'Price Mode' }}</h5>
                    <?php
                    $price_mode = $dataTypeRows->where('field', 'price_mode')->first();
                    ?>
                    {!! app('voyager')->formField($price_mode, $dataType, $dataTypeContent) !!}
                </div>
                <div id="price_mode_amount">
                    <div class="form-group">
                        <label for="status">{{ 'Price' }}</label>
                        @include('voyager::multilingual.input-hidden', [
                        '_field_name' => 'price',
                        '_field_trans' => get_field_translations($dataTypeContent, 'price')
                        ])
                        <input class="form-control" type="number" name="price"
                            value="{{ $dataTypeContent->price ?? '0' }}">
                    </div>
                    <div class="form-group">
                        <label for="status">{{ 'Discount' }}</label>
                        @include('voyager::multilingual.input-hidden', [
                        '_field_name' => 'discount',
                        '_field_trans' => get_field_translations($dataTypeContent, 'discount')
                        ])
                        <input class="form-control" type="number" name="discount"
                            value="{{ $dataTypeContent->discount ?? '0' }}">
                    </div>
                    <hr>
                    <div class="panel-heading">
                    </div>
                    <div class="form-group">
                        <label for="status">{{ 'Promotion Start Date' }}</label>
                        @include('voyager::multilingual.input-hidden', [
                        '_field_name' => 'promotion_start',
                        '_field_trans' => get_field_translations($dataTypeContent, 'promotion_start')
                        ])
                        <input type="text" class="form-control datepicker" id="promotion_start" name="promotion_start"
                            placeholder="promotion start" value="">
                    </div>
                    <div class="form-group">
                        <label for="status">{{ 'Promotion End Date' }}</label>
                        @include('voyager::multilingual.input-hidden', [
                        '_field_name' => 'promotion_end',
                        '_field_trans' => get_field_translations($dataTypeContent, 'promotion_end')
                        ])
                        <input type="text" class="form-control datepicker" id="promotion_end" name="promotion_end"
                            placeholder="promotion end" value="">
                    </div>
                </div>
                <div id="price_mode_text">
                    <div class="form-group">
                        <label for="status">{{ 'Costom text Price' }}</label>
                        <?php
                            $custom_price = $dataTypeRows->where('field', 'custom_price')->first();
                        ?>
                        {!! app('voyager')->formField($custom_price, $dataType, $dataTypeContent) !!}
                    </div>
                </div>
            </div>
        </div>
</div>
<div class="col-md-4">
    <!-- ### DETAILS ### -->
    <div class="panel panel panel-bordered panel-warning">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon wb-clipboard"></i> {{ 'Service Details' }}</h3>
            <div class="panel-actions">
                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="slug">{{ __('voyager::post.slug') }}</label>
                @include('voyager::multilingual.input-hidden', [
                '_field_name' => 'slug',
                '_field_trans' => get_field_translations($dataTypeContent, 'slug')
                ])
                <input type="text" class="form-control" id="slug" name="slug" placeholder="slug" {!!
                    isFieldSlugAutoGenerator($dataType, $dataTypeContent, "slug" ) !!}
                    value="{{ $dataTypeContent->slug ?? '' }}" data-db="{{ $dataType->name }}">
                <label id="slug-error-dup" class="dup-error" for="{{ "slug" }}"
                    style="display: none;color:red">{{ "slug ซ้ำ" }}</label>
                @push('custom-scripts')
                @include('javascript.slug-js');
                @endpush
            </div>
            <div class="form-group">
                <label for="status">{{ 'Service Status' }}</label>
                <select class="form-control" name="status">
                    <option value="PUBLISHED" @if(isset($dataTypeContent->status) &&
                        $dataTypeContent->status ==
                        'PUBLISHED')
                        selected="selected"@endif>{{ __('voyager::post.status_published') }}
                    </option>
                    <option value="DRAFT" @if(isset($dataTypeContent->status) &&
                        $dataTypeContent->status ==
                        'DRAFT')
                        selected="selected"@endif>{{ __('voyager::post.status_draft') }}</option>
                    <option value="PENDING" @if(isset($dataTypeContent->status) &&
                        $dataTypeContent->status ==
                        'PENDING')
                        selected="selected"@endif>{{ __('voyager::post.status_pending') }}
                    </option>
                </select>
            </div>
            <div class="form-group">
                <div class="controls">
                    <input id="featured" type="checkbox" name="featured" @if(isset($dataTypeContent->featured)
                    &&
                    $dataTypeContent->featured) checked="checked"@endif>
                    <label for="featured">{{ __('voyager::generic.featured') }}</label>
                </div>
            </div>
        </div>
    </div>
    <!-- ### IMAGE ### -->
    <div class="panel panel-bordered panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon wb-image"></i> {{ 'Service Image' }}</h3>
            <div class="panel-actions">
                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                @if(isset($dataTypeContent->image))
                <div data-field-name="image">
                    <a href="#" class="voyager-x remove-single-image" style="position:absolute;"></a>
                    <a class="fancybox" rel="group" href="{{ Voyager::image( $dataTypeContent->image ) }}">
                        <img src="@if( !filter_var($dataTypeContent->image, FILTER_VALIDATE_URL)){{ Voyager::image( $dataTypeContent->image ) }}@else{{ $dataTypeContent->image }}@endif"
                            data-file-name="{{ $dataTypeContent->image }}" data-id="{{ $dataTypeContent->getKey() }}"
                            style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:10px;">
                    </a>
                </div>
                @endif
                <input type="file" name="image" class="files-fileuploader">
            </div>
        </div>
    </div>
    <!-- ### SEO CONTENT ### -->
    <div class="panel panel-bordered panel-info">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon wb-search"></i>
                {{ __('voyager::post.seo_content') }}
            </h3>
            <div class="panel-actions">
                <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
            </div>
        </div>
        <div class="panel-body">
            <div class="form-group">
                <label for="seo_title">{{ __('voyager::post.seo_title') }}</label>
                @include('voyager::multilingual.input-hidden', [
                '_field_name' => 'seo_title',
                '_field_trans' => get_field_translations($dataTypeContent, 'seo_title')
                ])
                <input type="text" class="form-control" name="seo_title" placeholder="SEO Title"
                    value="{{ $dataTypeContent->seo_title ?? '' }}">
            </div>
            <div class="form-group">
                <label for="meta_description">{{ __('voyager::post.meta_description') }}</label>
                @include('voyager::multilingual.input-hidden', [
                '_field_name' => 'meta_description',
                '_field_trans' => get_field_translations($dataTypeContent, 'meta_description')
                ])
                <textarea class="form-control"
                    name="meta_description">{{ $dataTypeContent->meta_description ?? '' }}</textarea>
            </div>
            <div class="form-group">
                <label for="meta_keywords">{{ __('voyager::post.meta_keywords') }}</label>
                @include('voyager::multilingual.input-hidden', [
                '_field_name' => 'meta_keywords',
                '_field_trans' => get_field_translations($dataTypeContent, 'meta_keywords')
                ])
                <textarea class="form-control"
                    name="meta_keywords">{{ $dataTypeContent->meta_keywords ?? '' }}</textarea>
            </div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- ### WORKS  ### -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{ 'My Works of Image' }}</h3>
                <div class="panel-actions">
                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="status">{{ 'upload to your work' }}</label>
                    <input type="file" name="works" class="gallery_media">
                </div>
                <br>
                <div class="form-group">
                    @if(isset($dataTypeContent->works))
                    <?php $images = json_decode($dataTypeContent->works); ?>
                    @if($images != null)
                    @foreach($images as $image)
                    <div class="img_settings_container" data-field-name="works" style="float:left;padding-right:15px;">
                        <a href="#" class="voyager-x remove-multi-image" style="position: absolute;"></a>
                        {{-- <a class="fancybox" rel="group-multi" href="{{ Voyager::image( $image ) }}"> --}}
                        <img src="{{ Voyager::image( $image ) }}" data-file-name="{{ $image }}"
                            data-id="{{ $dataTypeContent->getKey() }}"
                            style="max-width:200px; height:auto; clear:both; display:block; padding:2px; border:1px solid #ddd; margin-bottom:5px;">
                        {{-- </a> --}}
                    </div>
                    @endforeach
                    @endif
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <!-- ### WORKS VIDEO ### -->
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">{{ 'My Works of Video' }}</h3>
                <div class="panel-actions">
                    <a class="panel-action voyager-angle-down" data-toggle="panel-collapse" aria-hidden="true"></a>
                </div>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="status">{{ 'add to your video link work' }}</label>
                    <br>
                    <code>Video URL (YouTube, Vimeo, Vine, Instagram, DailyMotion or Youku)
                    </code>
                    <?php
                        $works_video = $dataTypeRows->where('field', 'works_video')->first();
                    ?>
                </div>
                <div class="form-group">
                    {!! app('voyager')->formField($works_video, $dataType, $dataTypeContent) !!}
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<button type="submit" class="btn btn-primary pull-right">
    @if(isset($dataTypeContent->id)){{ __('voyager::post.update') }}@else <i class="icon wb-plus-circle"></i>
    {{ __('voyager::post.new') }} @endif
</button>
</form>
</div>
<div class="modal fade modal-danger" id="confirm_delete_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="voyager-warning"></i>
                    {{ __('voyager::generic.are_you_sure') }}</h4>
            </div>
            <div class="modal-body">
                <h4>{{ __('voyager::generic.are_you_sure_delete') }} '<span class="confirm_delete_name"></span>'
                </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default"
                    data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                <button type="button" class="btn btn-danger"
                    id="confirm_delete">{{ __('voyager::generic.delete_confirm') }}</button>
            </div>
        </div>
    </div>
</div>
<!-- End Delete File Modal -->
@stop
@section('javascript')
<!-- js -->
<script src="{{ asset('plugin/fileuploader/dist/jquery.fileuploader.min.js') }}" type="text/javascript"></script>
{{-- fancybox --}}
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js">
</script>
<script>
    var params = {};
        var $file;

        function deleteHandler(tag, isMulti) {
          return function() {
            $file = $(this).siblings(tag);

            params = {
                slug:   '{{ $dataType->slug }}',
                filename:  $file.data('file-name'),
                id:     $file.data('id'),
                field:  $file.parent().data('field-name'),
                multi: isMulti,
                _token: '{{ csrf_token() }}'
            }

            $('.confirm_delete_name').text(params.filename);
            $('#confirm_delete_modal').modal('show');
          };
        }

        $('document').ready(function () {
            $('.toggleswitch').bootstrapToggle();

            //Init datepicker for date fields if data-datepicker attribute defined
            //or if browser does not handle date inputs

            @if ($isModelTranslatable)
                $('.side-body').multilingual({"editing": true});
            @endif

            $('.side-body input[data-slug-origin]').each(function(i, el) {
                $(el).slugify();
            });

            $('.form-group').on('click', '.remove-multi-image', deleteHandler('img', true));
            $('.form-group').on('click', '.remove-single-image', deleteHandler('img', false));
            $('.form-group').on('click', '.remove-multi-file', deleteHandler('a', true));
            $('.form-group').on('click', '.remove-single-file', deleteHandler('a', false));

            $('#confirm_delete').on('click', function(){
                $.post('{{ route('voyager.'.$dataType->slug.'.media.remove') }}', params, function (response) {
                    if ( response
                        && response.data
                        && response.data.status
                        && response.data.status == 200 ) {

                        toastr.success(response.data.message);
                        $file.parent().fadeOut(300, function() { $(this).remove(); })
                    } else {
                        toastr.error("Error removing file.");
                    }
                });

                $('#confirm_delete_modal').modal('hide');
            });
            $('[data-toggle="tooltip"]').tooltip();
        });

        $(document).ready(function() {
            //fileuploader
            $('input.files-fileuploader').fileuploader({
                changeInput: '<div class="fileuploader-input">' +
                    '<div class="fileuploader-input-inner">' +
                        '<div class="fileuploader-icon-main"></div>' +
                        '<h3 class="fileuploader-input-caption"><span>${captions.feedback}</span></h3>' +
                        '<p>${captions.or}</p>' +
                        '<button type="button" class="fileuploader-input-button"><span>${captions.button}</span></button>' +
                        '</div>' +
                    '</div>',
                theme: 'dragdrop',
                maxSize: 1,
                fileMaxSize: 3,
                extensions: ['image/*'],
                limit: 1,
                quality:80,
            });

            $('input.gallery_media').fileuploader({
                limit: 12,
                maxSize: 20,
                addMore: true,
                sorter: {
                selectorExclude: null,
                placeholder: null,
                scrollContainer: window,
                onSort: function(list, listEl, parentEl, newInputEl, inputEl) {
                // onSort callback
                }
                }
            });
            //fancybox
            $(".fancybox").fancybox();
            //datetimepicker
            // $('#promotion_start').datetimepicker({
            //     format: 'dd/mm/yyyy'
            // });
            @php
                $date_s=date_create($dataTypeContent->promotion_start);
                $promotion_start = date_format($date_s,"m/d/Y H:i:s");
                $date_e=date_create($dataTypeContent->promotion_end);
                $promotion_end = date_format($date_e,"m/d/Y H:i:s");
            @endphp
            var promotion_start=  "{{ $dataTypeContent->promotion_start ? $promotion_start : '' }}";
            var promotion_end= "{{ $dataTypeContent->promotion_end ? $promotion_end : '' }}";
            $('#promotion_start').data("DateTimePicker").date(promotion_start);
            $('#promotion_end').data("DateTimePicker").date(promotion_end);

            //price_mode
            if($('#option-price-mode-amount').attr('checked')){
                $('#price_mode_text').hide();
                $('#price_mode_amount').show();
            }else{
                $('#price_mode_amount').hide();
                $('#price_mode_text').show();
            }
            var isChecked = $('#option-price-mode-amount').attr('checked')?true:false;

            $('input[type=radio][name=price_mode]').change(function() {
                if (this.value == 'amount') {
                    $('#price_mode_text').hide();
                    $('#price_mode_amount').show();
                }
                else if (this.value == 'text') {
                    $('#price_mode_amount').hide();
                    $('#price_mode_text').show();
                }
            });

            //video
            $(".select2").select2({
                tags: true,
                tokenSeparators: [',']
            })
        });
</script>
@stack('custom-scripts')
@stop
