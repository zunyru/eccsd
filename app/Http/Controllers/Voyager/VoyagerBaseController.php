<?php

namespace App\Http\Controllers\Voyager;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerBaseController as BaseVoyagerBaseController;

class VoyagerBaseController extends BaseVoyagerBaseController
{
    public function preview(Request $request,$slug_url = '')
    {

        $slug = $this->getSlugPreview($request);
        $perview = $this->getSlugPreviewCheck($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        if (strlen($dataType->model_name) != 0) {
            $model = app($dataType->model_name);

            // Use withTrashed() if model uses SoftDeletes and if toggle is selected
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $model = $model->withTrashed();
            }
            if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
                $model = $model->{$dataType->scope}();
            }
            $dataTypeContent = DB::table($dataType->name)
                ->where('slug', $slug_url)
                ->where('status', 'PUBLISHED')
                ->whereDate('public_at','<=',date('Y-m-d H:i:s'))
                ->first();
            if($perview == 'preview'){
                $dataTypeContent = DB::table($dataType->name)
                    ->where('slug', $slug_url)
                    ->first();
            }
            if(empty($dataTypeContent)){
                abort(404);
            }
            if ($dataTypeContent->deleted_at) {
                $isSoftDeleted = true;
            }
        } else {
            // If Model doest exist, get data from table name
            $dataTypeContent = DB::table($dataType->name)
                ->where('slug', $slug_url)
                ->where('status', 'PUBLISHED')
                ->whereDate('public_at','<=',date('Y-m-d H:i:s'))
                ->first();
            if($perview == 'preview'){
                $dataTypeContent = DB::table($dataType->name)
                    ->where('slug', $slug_url)
                    ->first();
            }
        }
        $dataTypeContent = $this->resolveRelations($dataTypeContent, $dataType, true);

        $related = DB::table($dataType->name)
            ->where('category_id', $dataTypeContent->category_id)
            ->where('slug','!=',$slug_url)
			->inRandomOrder()
            ->where('status', 'PUBLISHED')
            ->whereDate('public_at','<=',date('Y-m-d H:i:s'))
            ->limit(3)
            ->get();

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'read');

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        // Eagerload Relations
        $this->eagerLoadRelations($dataTypeContent, $dataType, 'read', $isModelTranslatable);

        $view = 'frontend.page-view-detail';

        return view($view, compact('dataType', 'dataTypeContent', 'isModelTranslatable','related'));
    }

    public function getSlugPreview(Request $request)
    {
        if (isset($this->slug)) {
            $slug = $this->slug;
        } else {
            $slug = explode('.', $request->route()->getName())[0];
        }

        return $slug;
    }

    public function getSlugPreviewCheck(Request $request)
    {
        if (isset($this->slug)) {
            $slug = $this->slug;
        } else {
            $slug = explode('.', $request->route()->getName())[1];
        }

        return $slug;
    }

}
