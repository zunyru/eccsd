<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function home()
    {
        $highlights = Page::orderBy('updated_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->limit(6)
            ->publiced()
            ->highlight(true)
            ->get();

        $pages = Page::orderBy('updated_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->limit(3)
            ->publiced()
            ->highlight(false)
            ->get();

        $view = 'frontend.home';

        return view($view, compact(
                'pages',
                'highlights')
        );
    }

    public function all(Request $request)
    {
        $pages = Page::orderBy('updated_at', 'DESC')
            ->orderBy('id', 'DESC')
            ->publiced()
            ->paginate(12);

        $view = 'frontend.all';

        return view($view, compact(
                'pages')
        );
    }
}
