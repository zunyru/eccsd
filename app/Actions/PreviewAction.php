<?php


namespace App\Actions;


use TCG\Voyager\Actions\AbstractAction;

class PreviewAction extends AbstractAction
{

    public function getTitle()
    {
        // TODO: Implement getTitle() method.
        return 'Preview';
    }

    public function getIcon()
    {
        // TODO: Implement getIcon() method.
        return 'voyager-browser';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-info preview',
            'target' => '_blank'
        ];
    }

    public function getDefaultRoute()
    {
        // TODO: Implement getDefaultRoute() method.
        return route($this->dataType->slug.'.preview', $this->data->slug);
    }

    public function shouldActionDisplayOnDataType()
    {
        return $this->dataType->slug == 'pages';
    }
}
