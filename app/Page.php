<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;

class Page extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    public function category()
    {
        return $this->hasOne(Category::class);
    }

    public function scopePubliced($query)
    {
        return $query->where('status', 'PUBLISHED')
            ->whereDate('public_at','<=',date('Y-m-d H:i:s'));
    }

    public function scopeHighlight($query,$enable = true)
    {
        return $query->where('highlight', $enable ?? false);
    }

    public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->author_id && Auth::user()) {
            $this->author_id = Auth::user()->getKey();
        }

        return parent::save();
    }

    public function authorId()
    {
        return $this->belongsTo(Voyager::modelClass('User'), 'author_id', 'id');
    }

}
