<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('frontend.home');
});

Route::get('/', [PageController::class, 'home']);

Route::get('/page-all', [PageController::class, 'all'])->name('page-all');

$namespacePrefix = '\\' . config('voyager.controllers.namespace') . '\\';

try {
    foreach (Voyager::model('DataType')::all() as $dataType) {
        $breadController = $dataType->controller
            ? Str::start($dataType->controller, '\\')
            : $namespacePrefix . 'VoyagerBaseController';

        Route::get($dataType->slug . '/{slug?}', $breadController . '@preview')->name($dataType->slug . '.detail');

        Route::get($dataType->slug . '/{slug}/preview', $breadController . '@preview')->name($dataType->slug . '.preview');
    }

    Route::get('pages/donate', 'Voyager\VoyagerBaseController@preview')->name('pages.donate');

} catch (\InvalidArgumentException $e) {
    throw new \InvalidArgumentException("Custom routes hasn't been configured because: " . $e->getMessage(), 1);
} catch (\Exception $e) {
    // do nothing, might just be because table not yet migrated.
}

