<?php

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    //roles update
    Route::match(['put', 'patch'], 'my-roles/{role}', 'Voyager\VoyagerRoleController@update')->name('voyager.roles.my-update');
    //setting
    Route::get('/websetting', 'Voyager\VoyagerSettingsController@websetting')->name('websetting.index');
    //summernote
    Route::post('summernote', 'Formfield\SummernoteController@upload');

    //slug
    Route::post('check/check-dup-title/', ['uses' => 'CheckduptitleController@checkDupTitle'])->name('check.check-dup-title');

    Route::post('check/check-dup-slug/', ['uses' => 'CheckduptitleController@checkDupSlug'])->name('check.check-dup-slug');

});
